# Named Pipe

- Thể hiện dưới dạng file không giống file thông thường và không có nội dung.
- Dữ liệu của một đường ống có tên được chứa trong bộ nhớ chứ không phải được ghi vào đĩa

` mkfifo my_pipe
ll my_pipe 
prw-rw-r-- 1 eva-hieunt eva-hieunt 0 Jun  9 11:47 my_pipe
`

`echo "Hello my_pipe" > my_pipe`

`Lưu ý: Loại tệp đặc biệt có chỉ định là p và độ dài của tệp bằng không.`

* Đường ống có tên chỉ được thông qua khi cả hai đầu của ống đã được mở ???
* Ta có thẻ input dữ liệu nhiều lần vào Named Pipe. Khi ta get output thì nội dung trong Named Pipe sẽ bị mất.