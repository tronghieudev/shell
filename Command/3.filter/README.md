# Filter

`sort`		Sắp xếp dữ liệu. <br>
`head`		In ra n dòng dữ liệu từ trên xuống.<br>
`tail`		In ra n dòng dữ liệu từ dưới lên.<br>
`wc`		Đếm ký tự, từ, dòng, byte.<br>
`sed`		Tìm kiếm và thay thế dữ liệu.<br>	
`uniq`		Loại bỏ các dòng trùng nhau.<br>
`awk`		Tìm kiếm và xử lý file text.<br>