# Redirect

- stdin: 0 : Standard Input
- stdout: 1 : Standard Output
- stderr: 2 : Standard Error

* Biểu tượng > được sử dụng chuyển hướng cho đầu ra (STDOUT). >> Thêm nội dung mà không ghi đè.
`command > file`

1. stdout > file: command 1> file `ll 1>des`
2. stderr > file: command 2> file `cat not_found.txt 2>des2`
3. stdout&stderr > file: command >file 2>&1 || command &>file
`bash Command/4.redirect/stdout_stderr_text.sh >des3 2>&1`


