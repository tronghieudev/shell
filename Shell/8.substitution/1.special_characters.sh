#!/bin/bash

a=10
echo -e "Value of \v a is $a \n"

# You can use the -E option to disable the interpretation of the backslash escapes (default).
# You can use the -n option to disable the insertion of a new line.
# -e option enables the interpretation of backslash escapes.