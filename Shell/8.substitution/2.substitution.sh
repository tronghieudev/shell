#!/bin/bash

DATE=`date`
echo "Date is $DATE"

USERS=`who | wc -l`
echo "Logged in user are $USERS"

UP=`date ; uptime`
echo "Uptime is $UP"

# Date is Thu Jun  9 14:28:16 +07 2022
# Logged in user are 1
# Uptime is Thu Jun  9 14:28:16 +07 2022
#  14:28:17 up 2 days,  5:47,  1 user,  load average: 5.30, 4.28, 3.18
