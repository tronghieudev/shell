#!/bin/bash

for i1 in 1 2; do
	for i2 in 1 2 3; do
		for i3 in 1 2; do
			if [[ $i2 == 2 ]]; then
				break 3
			fi

			echo "$i1 $i2 $i3"
		done
	done
done

#					  Condition $i2 == 2
# i1 	i2 	i3 		break	break2	break3
# 1		1	1						
# 1		1	2						
# 1		2	1		x		x		x
# 1		2	2		x		x		x
# 1		3	1				x		x
# 1		3	2				x		x
# 2		1	1						x
# 2		1	2						x
# 2		2	1		x		x		x
# 2		2	2		x		x		x
# 2		3	1				x		x
# 2		3	2				x		x
