#!/bin/bash

for num in {1..7}; do
	q=`expr $num % 2`
	if [[ $q -eq 0 ]]; then
		echo "Number is an even number!!"
      	continue
	fi

	echo "Found odd number"
done

# Found odd number
# Number is an even number!!
# Found odd number
# Number is an even number!!
# Found odd number
# Number is an even number!!
# Found odd number
