#!/bin/bash

val=`expr 10 + 5`

echo 'val=`expr 10 + 5`'
echo "Total value: $val"
echo ""

val=`expr 10 - 5`
echo 'val=`expr 10 - 5`'
echo "Total value: $val"
echo ""

val=`expr 10 \* 5`
echo 'val=`expr 10 \* 5`'
echo "Total value: $val"
echo ""

val=`expr 10 / 5`
echo 'val=`expr 10 / 5`'
echo "Total value: $val"
echo ""

val=`expr 10 % 5`
echo 'val=`expr 10 % 5`'
echo "Total value: $val"
echo ""

val=`expr 10 % 6`
echo 'val=`expr 10 % 6`'
echo "Total value: $val"
echo ""
