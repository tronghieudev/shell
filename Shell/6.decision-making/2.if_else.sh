#!/bin/bash

val="${1:-1}"
if [[ 1 == $val ]]; then
	echo 1;
else
	echo 2;
fi