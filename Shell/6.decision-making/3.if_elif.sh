#!/bin/bash

val=${1:-1}
if [[ 1 == $val ]]; then
	echo 1
elif [[ 2 == $val ]]; then
	echo 2
else
	echo 3
fi