# Encrypt

1. Mã hóa bất đối xứng (Asymmetric Encryption)
   - Thuật toán phổ biến: RSA, DSA, PKCS
   - Public key dùng để mã hóa (encrypt)
   - Private key dũng để giải mã (decrypt)
   - Attack:
     + Tấn công đứng giữa ( man in the middle attack)
     + Tấn công dựa trên thời gian
     + Tấn công lựa chọn thích nghi bản mã (adaptive chosen ciphertext attack)
       + PKCS: Một tiêu chuẩn chuyển đổi bản mã có khả năng kiểm tra tính hợp lệ của văn bản sau khi giải mã.
   - Cách encrypt & decrypt ???

3. Mã hóa đối xứng (Symmetric Encryption) 
   - Văn bản được mã hóa bằng một key khi gửi đi và người nhận xử dụng chính key này để giải mã dữ liệu.
   - Thuật toán phổ biến: AES-128, AES-192, AES-256
   - Cách encrypt & decrypt ???