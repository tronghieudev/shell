terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  access_key = var.access_key
  secret_key = var.secret_key
  region = var.region
}

resource "aws_vpc" "web" {
  tags = {
    Name = "vpc-01"
  }

  cidr_block = "10.10.0.0/16"
  instance_tenancy = "default"
}

resource "aws_subnet" "public_01" {
  vpc_id = aws_vpc.web.id
  cidr_block = "10.10.1.0/24"
  availability_zone = "us-east-1a"
  tags = {
    Name = "us-east-1a-public"
  }
}

resource "aws_subnet" "private_01" {
  vpc_id = aws_vpc.web.id
  cidr_block = "10.10.2.0/24"
  availability_zone = "us-east-1a"
  tags = {
    Name = "us-east-1a-private"
  }
}

resource "aws_internet_gateway" "vpc_igw_01" {
  vpc_id = aws_vpc.web.id

  tags = {
    Name = "vpc_igw_01"
  }
}

resource "aws_route_table" "vpc_rtb_public" {
  vpc_id = aws_vpc.web.id

  tags = {
    Name = "vpc_rtb_public"
  }

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.vpc_igw_01.id
  }
}

resource "aws_route_table" "vpc_rtb_private" {
  vpc_id = aws_vpc.web.id

  tags = {
    Name = "vpc_rtb_private"
  }
}

resource "aws_route_table_association" "vpc_rtb_public_ass" {
  route_table_id = aws_route_table.vpc_rtb_public.id
  subnet_id = aws_subnet.public_01.id
}

resource "aws_route_table_association" "vpc_rtb_private_ass" {
  route_table_id = aws_route_table.vpc_rtb_private.id
  subnet_id = aws_subnet.private_01.id
}

resource "aws_eip" "vpc_eip" {
  tags = {
    Name = "vpc elastic pi"
  }
}

resource "aws_nat_gateway" "vpc_nat_01" {
  subnet_id = aws_subnet.public_01.id
  connectivity_type= "public"
  allocation_id = aws_eip.vpc_eip.id
  depends_on = [aws_internet_gateway.vpc_igw_01]
  tags = {
    Name = "vpc NAT gw"
  }
}

resource "aws_vpc_endpoint" "vpc_enpoint_s3" {
  vpc_id       = aws_vpc.web.id
  service_name = "com.amazonaws.us-east-1.s3"
  vpc_endpoint_type = "Gateway"
  tags = {
    Name = "vpc_enpoint_s3"
  }
  route_table_ids = [aws_route_table.vpc_rtb_private.id]
  policy = <<POLICY
    {
        "Statement": [
            {
                "Effect": "Allow",
                "Principal": "*",
                "Action": "*",
                "Resource": "*",
                "Sid":""
            }
      ],
      "Version": "2008-10-17"
    }
    POLICY
}

resource "aws_security_group" "web" {
  vpc_id = aws_vpc.web.id
  name = "web-sg-group"
  description = "Web security"
  tags = {
    Name = "Web"
  }

  ingress {
    from_port = 22
    protocol  = "tcp"
    to_port   = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 80
    protocol  = "tcp"
    to_port   = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 443
    protocol  = "tcp"
    to_port   = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    protocol  = "-1"
    to_port   = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_key_pair" "my_key" {
  key_name = "ssh-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDYc+W3P09Lk4QjUUc4KZzlscDvN+w0BdLLaTrP3Wm0O6cHYd6b1TFW/UXw8cf6SpyuGDuxYYoqh5WMdZehrq7jn4qYK+aEynPXCYLPf71uF+RGYNuKV1+7mmLBVZT1soamHiRXpVAsUzPYf+V6msjhtLuOnW6DqTG40RppNqdkH/FCqc4DrcXI0GlO9LyUoo9Px0279xOzPw0dm22wJ5NZL4kO2YCTv4JFKqrEIfwMHVWBr60sz7f2yuWT8ix3kZOG2wvVEDX7Yq/7bpeLU5HHCC7NnyTBqlBlE/ZCdVgpN7p8h1JBxojKLEjHWl1uatKGjGwAKwwK6r9mIhC5E3Dj htvhieunt@CPY004026"
}

resource "aws_instance" "web_public" {
  ami = "ami-0cff7528ff583bf9a"
  instance_type = "t2.micro"
  subnet_id = aws_subnet.public_01.id
  associate_public_ip_address = true
  security_groups = [aws_security_group.web.id]
  key_name = "ssh-key"
  tags = {
    Name = "Web public"
  }
}

resource "aws_instance" "db_private" {
  ami = "ami-0cff7528ff583bf9a"
  instance_type = "t2.micro"
  subnet_id = aws_subnet.private_01.id
  associate_public_ip_address = false
  security_groups = [aws_security_group.web.id]
  key_name = "ssh-key"
  tags = {
    Name = "DB private"
  }
}

#resource "aws_network_interface" "test" {
#  subnet_id = aws_subnet.private_01.id
#  private_ips = ["10.10.2.10"]
#  security_groups = [aws_security_group.web.id]
#
#  attachment {
#    device_index = 1
#    instance     = aws_instance.db_private.id
#  }
#}