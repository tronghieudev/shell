terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  access_key = var.access_key
  secret_key = var.secret_key
  region = var.region
}

resource "aws_vpc" "web" {
  cidr_block = "10.1.0.0/16"

  tags = {
    Name = "Web"
  }
}

resource "aws_subnet" "web" {
  vpc_id      = aws_vpc.web.id
  cidr_block  = "10.1.1.0/24"
  availability_zone =  "us-east-1a"
  tags = {
    Name = "web"
  }
}


resource "aws_network_interface" "web" {
  subnet_id   = aws_subnet.web.id
  private_ips = ["10.1.1.10"]

  tags = {
    Name = "primary_network_interface"
  }
}

resource "aws_network_interface_sg_attachment" "web" {
  network_interface_id = aws_network_interface.web.id
  security_group_id    = aws_security_group.web.id
}

resource "aws_security_group" "web" {
  vpc_id = aws_vpc.web.id

  ingress {
    from_port = 80
    protocol  = "tcp"
    to_port   = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    protocol  = "tcp"
    to_port   = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Web"
  }
}

resource "aws_instance" "web" {
  ami = "ami-0cff7528ff583bf9a"
  instance_type = "t2.micro"

  tags = {
    Name = "Web"
  }

  network_interface {
    device_index         = 0
    network_interface_id = aws_network_interface.web.id
  }

  user_data = file("./init_instance.sh")
}
