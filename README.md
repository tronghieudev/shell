# Docker
- Shell vs exec form: https://emmer.dev/blog/docker-shell-vs.-exec-form
- Deamon: https://devopsz.com/docker-101-part-3


# Linux
- Daemon: https://quantrimang.com/daemon-la-gi-184509
- Foceground vs Background: https://quantrimang.com/quan-ly-tien-trinh-trong-unix-linux-156592
- STDERR tới STDOUT: https://khaidantri.net/stdin-stdout-va-stderr-tren-linux-la-gi


# Shell
- Here Document << EOF: 
https://www.baeldung.com/linux/heredoc-herestring <br>
https://hocdevops.com/commands/bash-heredoc

- Bash: 
https://mywiki.wooledge.org/Bashism
https://helpex.vn/question/su-khac-biet-giua-sh-va-bash-5cb022c5ae03f645f420985d


# More
- Daemon: Disk and excution monitor